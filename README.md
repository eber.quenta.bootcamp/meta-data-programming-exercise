# MetaData Programming
This project implements an exercise using:
- typescript
- decorators
- Reflects
- ..others

## Start
After clone the project:

```
yarn install
```

then

```
yarn start
```

Then you can run the project in the program in `dist/index.js` with `node dist/index.js`