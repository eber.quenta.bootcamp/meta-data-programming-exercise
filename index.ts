import "reflect-metadata"
/* DECORATORS IN ROUTES */
// Define a Decorator 
const ROUTE = Symbol("RouteName")
const JSON_ATTR = Symbol("simboloJson")
const JSON_TYPE = Symbol("simboloJson")

const Route = (name: string): ClassDecorator => {
  return target => {
    Reflect.defineMetadata(ROUTE, name, target)
  }
}

const JsonAttribute = (name: string, type: 'string' | 'number' | 'boolean'): PropertyDecorator => {
  return (target, propertyKey) => {
    Reflect.defineMetadata(JSON_ATTR, name, target, propertyKey)
    Reflect.defineMetadata(JSON_TYPE, type, target, propertyKey)
  }
}

// Use decotarors
@Route('/api/pets')
class PetController {
  @JsonAttribute('name', 'string')
  name: string = 'Gomita '

  @JsonAttribute('age', 'number')
  age: number = 1
}
const doby = new PetController()

const calculateRoute = function (petCtrl: PetController) {
  const petPrototype = Object.getPrototypeOf(petCtrl).constructor
  // console.log('Class ->', petPrototype)

  const metaKeys = Reflect.getMetadataKeys(petPrototype)
  // console.log('metaKeys ->', metaKeyss)
  const route = Reflect.getMetadata(ROUTE, petPrototype)
  // console.log('Route ->', route)
}

calculateRoute(doby)

/* SERIALIZATION WITH DECORATORS AND METADATA */

function desiarizable<T>(jsonParam: string, type: new () => T): T {
  const jsonObj = JSON.parse(jsonParam)
  const instanceType = new type()
  const properties = Object.keys(instanceType as any)
  properties.forEach(key => {
    const jsonAttr = Reflect.getMetadata(JSON_ATTR, instanceType, key)
    const typeAttr = Reflect.getMetadata(JSON_TYPE, instanceType, key)
    if (jsonAttr) {
      if (typeof jsonObj[key] != typeAttr) {
        throw new Error(`The type of ${jsonAttr} is with a wrong type`)
      }
      (<any>instanceType)[key] = jsonObj[key]
    }
  })
  return instanceType
}
const jsonString = '{ "name": "Gomita", "age": 2}'
console.log(desiarizable(jsonString, PetController))
